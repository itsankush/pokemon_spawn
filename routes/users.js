var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
const app = require('../app');

// get all users
router.get('/', async function(req, res, next) {
  try {
    var rows, fields;
    
    [rows, fields] = await res.locals.connection.execute('SELECT * FROM users');

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.get('/:id', async function(req, res, next) {
  try {
    var rows, fields;
    
    [rows, fields] = await res.locals.connection.execute('SELECT * FROM users WHERE id=' + req.params.id);

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.get('/pokemon-detail/:id', async function(req, res, next) {
  try {
    var rows, fields;
    
    [rows, fields] = await res.locals.connection.execute('SELECT * FROM pokemons WHERE user=' + req.params.id);
    
    var pokemonDetails = [];
    for (var i = 0; i < rows.length; i ++) {
      var response = await fetch('https://pokeapi.co/api/v2/pokemon/' + rows[i]['number'], {
        method: 'GET'
      });
      var detail = await response.json();

      pokemonDetails.push(detail);
    }

    res.send(JSON.stringify({ "status": 200, "error": null, "response": pokemonDetails }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.post('/', async function(req, res, next) {
  try {
    // get pokemon data by number
    var response = await fetch('https://pokeapi.co/api/v2/pokemon/' + req.body.pokemon, {
      method: 'GET'
    });
    var pokemonData = await response.json();
    var pokemonImage = 'https://play.pokemonshowdown.com/sprites/xyani/' + pokemonData.name + '.gif';

    var rows, fields;
    
    // add user
    [rows, fields] = await res.locals.connection.execute("INSERT INTO users (username, password, try) VALUES ('" + req.body.username + "', '" + req.body.password + "', 3);");
    // get id of added user
    [rows, fields] = await res.locals.connection.execute("SELECT LAST_INSERT_ID();");
    var user = rows[0]['LAST_INSERT_ID()'];

    // add pokemon
    [rows, fields] = await res.locals.connection.execute("INSERT INTO pokemons (number, name, image, user) VALUES (" + req.body.pokemon + ", '" + pokemonData.name + "', '" + pokemonImage + "', " + user + ");");

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.put('/:id', async function(req, res, next) {
  try {
    var rows, fields;

    [rows, fields] = await res.locals.connection.execute("UPDATE users SET username='" + req.body.username + "', password='" + req.body.password + "', try=" + req.body.try + " WHERE id=" + req.params.id);

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.put('/catch/:id', async function(req, res, next) {
  try {
    // check if spawn pokemon exist
    var spawnPokemon = req.app.get('spawn_pokemon');
    if (spawnPokemon == null) {
      res.send(JSON.stringify({ "status": 200, "error": null, "response": "No avaliable pokemon" }));
      return;
    }
    
    var rows, fields;

    // get user data
    [rows, fields] = await res.locals.connection.execute('SELECT * FROM users WHERE id=' + req.params.id);

    var tryCount = rows[0]['try'];
    if (tryCount == 0) {
      res.send(JSON.stringify({ "status": 200, "error": null, "response": "Can not try anymore" }));
      return;
    }

    // check if name is correct
    if (spawnPokemon.name == req.body.name) {
      // add spawn pokemon with user id
      [rows, fields] = await res.locals.connection.execute("INSERT INTO pokemons (number, name, image, user) VALUES (" + spawnPokemon.number + ", '" + spawnPokemon.name + "', '" + spawnPokemon.image + "', " + req.params.id + ");");
      req.app.set('spawn_pokemon', null);

      res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));      
    } else {
      // decrease try
      tryCount --;
      [rows, fields] = await res.locals.connection.execute("UPDATE users SET try=" + tryCount + " WHERE id=" + req.params.id);

      res.send(JSON.stringify({ "status": 200, "error": null, "response": "Pokemon name is wrong" }));
    }

  } catch (err) {
    console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

// delete all users
router.delete('/', async function(req, res, next) {
  try {
    var rows, fields;
    
    [rows, fields] = await res.locals.connection.execute("DELETE FROM users");

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

router.delete('/:id', async function(req, res, next) {
  try {
    var rows, fields;
    
    [rows, fields] = await res.locals.connection.execute("DELETE FROM users WHERE id=" + req.params.id);

    res.send(JSON.stringify({ "status": 200, "error": null, "response": rows }));
  } catch (err) {
    //console.log(err);
    res.send(JSON.stringify({ "status": 500, "error": err, "response": null }));
  }
});

module.exports = router;
