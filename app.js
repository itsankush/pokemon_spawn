var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql2/promise');
var fetch = require('node-fetch');
var jimp = require('jimp');
var gifwrap = require('gifwrap');
var imageDownloader = require('image-downloader');
var fs = require('fs');
var FormData = require('form-data');

var parse = require('./modules/parse');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var pokemonsRouter = require('./routes/pokemons');

var app = express();

var CONFIG = require('./config/config');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(async function(req, res, next) {
  res.locals.connection = await mysql.createConnection({
    host: CONFIG.db_host,
    user: CONFIG.db_user,
    password: CONFIG.db_password,
    database: CONFIG.db_name
  });

  //res.locals.connection.connect();
  next();
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/v1/users', usersRouter);
app.use('/api/v1/pokemons', pokemonsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

require('./modules/spawn');

// spawn pokemons
setInterval(
  async function() {
    try {
      // get random pokemon data
      var number = Math.floor(Math.random() * 152) - 1;
      var response = await fetch('https://pokeapi.co/api/v2/pokemon/' + number, {
          method: 'GET'
      });
      var pokemonData = await response.json();
      var pokemonImage = 'https://play.pokemonshowdown.com/sprites/xyani/' + pokemonData.name + '.gif';
      

      // get darken image
      /*
      console.log(pokemonImage);
      var image = await jimp.read(pokemonImage);
      image.greyscale();
      //image.color([{ apply: 'darken', params: [100]}]);
      image.write('image.gif');
      console.log('done!');
      */
      await imageDownloader.image({
        url: pokemonImage,
        dest: 'image0.gif'
      });

      gifwrap.GifUtil.read('image0.gif').then(inputGif => {
      
        inputGif.frames.forEach(frame => {
    
          const buf = frame.bitmap.data;
          frame.scanAllCoords((x, y, bi) => {
  
            const r = buf[bi];
            const g = buf[bi + 1];
            const b = buf[bi + 2];
            const a = buf[bi + 3];

            buf[bi] = 0x00;
            buf[bi + 1] /= 0x00;
            buf[bi + 2] /= 0x00;
          });
        });
    
        // Pass inputGif to write() to preserve the original GIF's specs.
        return gifwrap.GifUtil.write("image.gif", inputGif.frames, inputGif).then(outputGif => {
            console.log("modified");
        });
      });
     
      // get hint
      var hint = parse.getHint(pokemonData.name);

      // send darken image and hint to front-end
      var formData = new FormData();
      formData.append('image', fs.createReadStream('image.gif'));
      formData.append('hint', hint);

      /*var response = await fetch('https://localhost:3000', {
          method: 'POST',
          body: formData
      });*/

      // save spawn pokemon
      console.log(pokemonData.name);
      app.set('spawn_pokemon', { 'number': number, 'name': pokemonData.name, 'image': pokemonImage  })

      setTimeout(async function() {
        // remove pokemon if not catch
        app.set('spawn_pokemon', null);
      }, 1000 * CONFIG.life_time);
    } catch (err) {
      console.log(err);
    }
  },
  1000 * CONFIG.spawn_circle
);

module.exports = app;
