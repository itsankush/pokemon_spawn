function getHint(name) {
    var showCount = name.length / 2 > 3? 3: name.length / 2;
    var indexes = Array(name.length);
    var hint = '_'.repeat(name.length);

    for (var i = 0; i < name.length; i ++)
        indexes[i] = i;

    for (var i = 0; i < showCount; i ++) {
        var randomIndex = Math.floor(Math.random() * indexes.length);
        var showIndex = indexes[randomIndex];
        
        indexes.splice(randomIndex, 1);
        hint = setCharAt(hint, showIndex, name.charAt(showIndex));
    }

    return hint;
}

function setCharAt(str, index, chr) {
    if(index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}

module.exports = {
    getHint
};